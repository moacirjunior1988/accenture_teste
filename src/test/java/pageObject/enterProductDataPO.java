package pageObject;

import org.openqa.selenium.WebElement;

import enums.byElements;
import general.baseTest;

public class enterProductDataPO extends baseTest
{
	//Elements
	private static WebElement txbStartDate = GetElement(byElements.Id, "startdate");
	private static WebElement cbxInsuranceSum = GetElement(byElements.Id, "insurancesum");
	private static WebElement cbxMeritRating = GetElement(byElements.Id, "meritrating");
	private static WebElement cbxDamageInsurance = GetElement(byElements.Id, "damageinsurance");
	private static WebElement chbOptionalProductsEuroProtection = GetElement(byElements.Id, "EuroProtection");
	private static WebElement chbOptionalProductsLegalDefenseInsurance = GetElement(byElements.Id, "LegalDefenseInsurance");
	private static WebElement cbxCourtesyCar = GetElement(byElements.Id, "courtesycar");	
	private static WebElement btnNext = GetElement(byElements.Id, "nextselectpriceoption");
	private static WebElement btnPrev = GetElement(byElements.Id, "preventerinsurancedata");
	
	//SendKeys
	public static void startDate(String text)
	{
		Sendkeys(txbStartDate, text);
	}
	
	// ComboBox
	public static void insuranceSu(String text)
	{
		ComboBoxSelect(cbxInsuranceSum, text);
	}
	
	public static void meritRating(String text)
	{
		ComboBoxSelect(cbxMeritRating, text);
	}
	
	public static void damageInsurance(String text)
	{
		ComboBoxSelect(cbxDamageInsurance, text);
	}
	
	public static void courtesyCar(String text)
	{
		ComboBoxSelect(cbxCourtesyCar, text);
	}
	
	//CheckBox
	public static void optionalProductsEuroProtection()
	{
		ClicksJS(chbOptionalProductsEuroProtection);
	}
	
	public static void optionalProductsLegalDefenseInsurance()
	{
		ClicksJS(chbOptionalProductsLegalDefenseInsurance);
	}

	//Click
	public static void next()
	{
		Clicks(btnNext);
	}
	
	public static void prev()
	{
		Clicks(btnPrev);
	}

	//Validações
	public static Boolean validarAExibicaoDaAbaEnterProductData()
	{
		return baseTest.ValidacoesDeElementos(txbStartDate);
	}
}
