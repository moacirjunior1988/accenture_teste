package steps;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentTest;

import cucumber.api.Scenario;
import cucumber.api.java.pt.Dado;
import general.baseTest;
import general.hooks;
import general.testConfiguration;
import general.testMassa;
import io.github.bonigarcia.wdm.WebDriverManager;

public class acessarSiteSteps 
{
	protected ExtentTest extentLog;
	private testMassa testMassa;
	private testConfiguration testConfiguration;
	
	public acessarSiteSteps(testMassa testMassa, testConfiguration testConfiguration)
	{
		this.testMassa = testMassa;
		this.testConfiguration = testConfiguration;
		this.extentLog =  hooks.extentTest;;
	}
	
	@Dado ("^que acesso o site$")
	public void queAcessoOSite()
	{
		WebDriverManager.chromedriver().setup();
		baseTest.webDriver = new ChromeDriver();
		baseTest.wait = new WebDriverWait(baseTest.webDriver, 200);
		baseTest.webDriver.navigate().to(testConfiguration.url);
		baseTest.webDriver.manage().window().maximize();
	}
}
