package steps;

import java.util.List;
import java.util.Map;

import com.aventstack.extentreports.ExtentTest;

import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import general.baseTest;
import general.extentReportsManager;
import general.hooks;
import general.testConfiguration;
import general.testMassa;
import pageObject.enterInsurantDataPO;
import pageObject.enterVehicleDataPO;

public class enterInsurantDataSteps 
{
	protected ExtentTest extentLog;
	private testMassa testMassa;
	private testConfiguration testConfiguration;
	private enterInsurantDataPO enterInsurantDataPO;
	
	public enterInsurantDataSteps(testMassa testMassa, testConfiguration testConfiguration, 
			enterInsurantDataPO enterInsurantDataPO)
	{
		this.testMassa = testMassa;
		this.testConfiguration = testConfiguration;
		this.extentLog =  hooks.extentTest;
		this.enterInsurantDataPO = enterInsurantDataPO;
	}
	
	@Dado("^que estou na aba enter insurant data$")
	public void dadoQueEstouNoAbaEnterInsutantData()
	{
		extentReportsManager.SalvarLogSucesso("Dado que estou na aba enter insurant data");
		if(!enterInsurantDataPO.validarAExibicaoDaAbaEnterInsurantData()) baseTest.webDriver.quit();
		else extentReportsManager.SalvarLogScreenShotSucesso("Valida��o da exibi��o da aba enter insurant data");
	}
	
	@E("^iniciarei o preenchendo o campo first name$")
	public void eIniciareiOPreenchendoOCampoFirstName(DataTable dataTable)
	{
		extentReportsManager.SalvarLogSucesso("E iniciarei o preenchendo o campo first name");
		List<Map<String,String>> data = dataTable.asMaps(String.class, String.class);
		
		enterInsurantDataPO.firstName(data.get(0).get("first_name"));
	}
	
	@E("^preencherei o campo last name$")
	public void ePreenchereiOCampoLastName(DataTable dataTable)
	{
		extentReportsManager.SalvarLogSucesso("E preencherei o campo last name");
		List<Map<String,String>> data = dataTable.asMaps(String.class, String.class);
		
		enterInsurantDataPO.lastName(data.get(0).get("last_name"));
	}
	
	@E("^preencherei o campo date of birth$")
	public void ePreenchereiOCampoDateOfBirth(DataTable dataTable)
	{
		extentReportsManager.SalvarLogSucesso("E preencherei o campo date of birth");
		List<Map<String,String>> data = dataTable.asMaps(String.class, String.class);
		
		enterInsurantDataPO.dateOfBirth(data.get(0).get("date_of_birth"));
	}
	
	@E("^preencherei o campo gender$")
	public void ePreenchereiOCampoGender(DataTable dataTable)
	{
		extentReportsManager.SalvarLogSucesso("E preencherei o campo gender");
		List<Map<String,String>> data = dataTable.asMaps(String.class, String.class);
		
		if(data.get(0).get("gender").equals("male")) enterInsurantDataPO.genderMale();
		else if (data.get(0).get("gender").equals("female")) enterInsurantDataPO.genderFemale();
	}
	
	@E("^preencherei o campo street address$")
	public void ePreenchereiOCampoStreetAddress(DataTable dataTable)
	{
		extentReportsManager.SalvarLogSucesso("E preencherei o campo street address");
		List<Map<String,String>> data = dataTable.asMaps(String.class, String.class);
		
		enterInsurantDataPO.streetAddress(data.get(0).get("stress_address"));
	}
	
	@E("^preencherei o campo country$")
	public void ePreenchereiOCampoCountry(DataTable dataTable)
	{
		extentReportsManager.SalvarLogSucesso("E preencherei o campo country");
		List<Map<String,String>> data = dataTable.asMaps(String.class, String.class);
		
		enterInsurantDataPO.country(data.get(0).get("country"));
	}
	
	@E("^preencherei o campo zip code$")
	public void ePreenchereiOCampoZipCode(DataTable dataTable)
	{
		extentReportsManager.SalvarLogSucesso("E preencherei o campo zip code");
		List<Map<String,String>> data = dataTable.asMaps(String.class, String.class);
		
		enterInsurantDataPO.zipCode(data.get(0).get("zip_code"));
	}
	
	@E("^preencherei o campo city$")
	public void ePreenchereiOCampoCity(DataTable dataTable)
	{
		extentReportsManager.SalvarLogSucesso("E preencherei o campo city");
		List<Map<String,String>> data = dataTable.asMaps(String.class, String.class);
		
		enterInsurantDataPO.city(data.get(0).get("city"));
	}
	
	@E("^preencherei o campo occupation$")
	public void ePreenchereiOCampoOccupation(DataTable dataTable)
	{
		extentReportsManager.SalvarLogSucesso("E preencherei o campo occupation");
		List<Map<String,String>> data = dataTable.asMaps(String.class, String.class);
		
		enterInsurantDataPO.occupation(data.get(0).get("occupation"));
	}
	
	@E("^preencherei o campo hobbies$")
	public void ePreenchereiOCampoHobbies(DataTable dataTable)
	{
		extentReportsManager.SalvarLogSucesso("E preencherei o campo hobbies");
		List<Map<String,String>> data = dataTable.asMaps(String.class, String.class);
		
		String[] hobbies = data.get(0).get("hobbies").split(",");
		
		for(int a = 0; a <= (hobbies.length - 1); a++)
		{
			switch (hobbies[a]) 
			{
				case "Speeding":
					enterInsurantDataPO.hobbiesSpeeding();
					break;
					
				case "Bungee Jumping":
					enterInsurantDataPO.hobbiesBungeeJumping();
					break;
					
				case "Cliff Diving":
					enterInsurantDataPO.hobbiesCliffDiving();
					break;
					
				case "Skydiving":
					enterInsurantDataPO.hobbiesSkydiving();
					break;
					
				case "Other":
					enterInsurantDataPO.hobbiesOther();
					break;
			}
		}
	}

	@E("^preencherei o campo web site$")
	public void ePreenchereiOCampoWebSite(DataTable dataTable)
	{
		extentReportsManager.SalvarLogSucesso("E preencherei o campo web site");
		List<Map<String,String>> data = dataTable.asMaps(String.class, String.class);
		
		enterInsurantDataPO.webSite(data.get(0).get("web_site"));
	}
	
	@E("^preencherei o campo picture$")
	public void ePreenchereiOCampoPicture(DataTable dataTable)
	{
		extentReportsManager.SalvarLogSucesso("E preencherei o campo picture");
		List<Map<String,String>> data = dataTable.asMaps(String.class, String.class);
		
		//enterInsurantDataPO.pictureOpen();
	}
	
	@E("^clico em next aba enter insurant data$")
	public void eClicoEmNextAbaEnterInsurantData()
	{
		extentReportsManager.SalvarLogSucesso("E clico em next aba enter insurant data");

		enterInsurantDataPO.next();
	}
}
