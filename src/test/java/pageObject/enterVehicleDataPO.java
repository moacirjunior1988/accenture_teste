package pageObject;

import org.openqa.selenium.WebElement;
import enums.byElements;
import general.baseTest;

public class enterVehicleDataPO extends baseTest
{
	//Elements
	private static WebElement cbxMake = GetElement(byElements.Id, "make");
	private static WebElement cbxModel = GetElement(byElements.Id, "model");
	private static WebElement txbCylinderCapacity = GetElement(byElements.Id, "cylindercapacity");
	private static WebElement txbEnginePerformance = GetElement(byElements.Id, "engineperformance");
	private static WebElement txbDateOfManufacture = GetElement(byElements.Id, "dateofmanufacture");
	private static WebElement cbxNumberOfSeats = GetElement(byElements.Id, "numberofseats");
	private static WebElement rbnRightHandDriveYes = GetElement(byElements.Id, "righthanddriveyes");
	private static WebElement rbnRightHandDriveNo = GetElement(byElements.Id, "righthanddriveno");
	private static WebElement cbxNumberOsSeatsMotorcycle = GetElement(byElements.Id, "numberofseatsmotorcycle");
	private static WebElement cbxFuelType = GetElement(byElements.Id, "fuel");
	private static WebElement txbPayload = GetElement(byElements.Id, "payload");
	private static WebElement txbTotalWeight = GetElement(byElements.Id, "totalweight");
	private static WebElement txbListPrice = GetElement(byElements.Id, "listprice");
	private static WebElement txbLicensePlateNumber = GetElement(byElements.Id, "licenseplatenumber");
	private static WebElement txbAnnualMileage = GetElement(byElements.Id, "annualmileage");
	private static WebElement btnNext = GetElement(byElements.Id, "nextenterinsurantdata");
	
	//Methods
	
	//SendKeys
	public static void cylinderCapacity(String text)
	{
		Sendkeys(txbCylinderCapacity, text);
	}
	
	public static void enginePerformance(String text)
	{
		Sendkeys(txbEnginePerformance, text);
	}
	
	public static void dateOfManufacture(String text)
	{
		Sendkeys(txbDateOfManufacture, text);
	}
	
	public static void payload(String text)
	{
		Sendkeys(txbPayload, text);
	}
	
	public static void totalWeight(String text)
	{
		Sendkeys(txbTotalWeight, text);
	}
	
	public static void listPrice(String text)
	{
		Sendkeys(txbListPrice, text);
	}
	
	public static void licensePlateNumber(String text)
	{
		Sendkeys(txbLicensePlateNumber, text);
	}
	
	public static void annualMileage(String text)
	{
		Sendkeys(txbAnnualMileage, text);
	}
	
	//ComboBox
	public static void make(String text)
	{
		ComboBoxSelect(cbxMake, text);
	}
	
	public static void model(String text)
	{
		ComboBoxSelect(cbxModel, text);
	}
	
	public static void numberOfSeats(String text)
	{
		ComboBoxSelect(cbxNumberOfSeats, text);
	}
	
	public static void numberOsSeatsMotorcycle(String text)
	{
		ComboBoxSelect(cbxNumberOsSeatsMotorcycle, text);
	}
	
	public static void fuelType(String text)
	{
		ComboBoxSelect(cbxFuelType, text);
	}
	
	//RadioButton
	public static void rightHandDriveYes()
	{
		ClicksJS(rbnRightHandDriveYes);
	}
	
	public static void rightHandDriveNo()
	{
		ClicksJS(rbnRightHandDriveNo);
	}

	//Click
	public static void next()
	{
		Clicks(btnNext);
	}

	//Validações
	public static Boolean validarAExibicaoDaAbaEnterVehicleData()
	{
		return baseTest.ValidacoesDeElementos(cbxMake);
	}
}
