package general;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentTest;

import enums.byElements;

public class baseTest 
{
	//Variaveis Globais
	public static WebDriver webDriver;
	public static WebDriverWait wait;
    public static Actions action;
    private static By by;
	private static Properties props = new Properties();
	private static Random random;
	private static LocalDateTime dataAtual;
	private static Integer mesAtual;
	protected ExtentTest extentLog;
	
	//Method Instanciando a propria classe.
	public baseTest()
	{
		this.extentLog = hooks.extentTest;
	}
	
	//Method de sleep
	public static void Sleep(long timeOut) 
	{
		try {
			Thread.sleep(timeOut);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//Method de finalizar os processos do navegador
	public static boolean ExecutarCMD(String processo) 
    {
        try {
            String line;
            Process p = Runtime.getRuntime().exec("taskkill /im chromedriver.exe /f /t");
            BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
            while ((line = input.readLine()) != null) {
                if (!line.trim().equals("")) {
                    if (line.substring(1, line.indexOf("\"", 1)).equalsIgnoreCase(processo)) {
                        Runtime.getRuntime().exec("taskkill /F /IM " + line.substring(1, line.indexOf("\"", 1)));
                        return true;
                    }
                }
            }
            input.close();
        } catch (Exception err) {
            err.printStackTrace();
        }
        return false;
    }

	//M�todo de carregar o arquivo de parametros e devolver para parametro foi solicitado
	public static String GetProp(String file, String parameter)
	{
		String CaminhoPropriedades = System.getProperty("user.dir") +
				"\\src\\test\\resources\\" + file + ".properties";
		
		try {
			props.load(new FileInputStream(CaminhoPropriedades));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return props.getProperty(parameter);
	}

	//M�todo de criar pasta n�o exist�nte
	public static void criarPasta(String caminhoPastaCriar)
	{
		File pasta = new File(caminhoPastaCriar);
		if(!pasta.exists())
		{
			pasta.mkdirs();
		}
	} 

	//M�todo de gerador de idade dinamico
	public static String geradorIdade()
	{
		random = new Random();
		int idade = random.nextInt(80), idadeFaltante = 0;
		
		if(idade < 18) 
		{
			idadeFaltante = 18 - idade;
			idade = idade + idadeFaltante;
		}
		
		return Integer.toString(idade);
	}
	
	public static String diaAtual()
    {
    	String  dia = "";
    	dataAtual = LocalDateTime.now();

    	if(dataAtual.getDayOfMonth() < 10)
    	{
    		dia = "0" + dataAtual.getDayOfMonth();
    	}
    	else
    	{
    		dia = Integer.toString(dataAtual.getDayOfMonth());
    	}
    	
    	return dia;
    }
    
    public static String mesAtual()
    {
    	String  mes = "";
    	dataAtual = LocalDateTime.now();
    	mesAtual = dataAtual.getMonth().getValue();
    	
    	if(mesAtual < 10)
    	{
    		mes = "0" + mesAtual;
    	}
    	else
    	{
    		mes = Integer.toString(mesAtual);
    	}
    	
    	return mes;
    }
    
    public static String anoAtual()
    {
    	dataAtual = LocalDateTime.now();
    	return Integer.toString(dataAtual.getYear());
    }

    public static String horaAtual()
    {
    	String hora = "";
    	dataAtual = LocalDateTime.now();
    	
    	if(dataAtual.getHour() < 10)
    	{
    		hora = "0" + Integer.toString(dataAtual.getHour());
    	}
    	else
    	{
    		hora = Integer.toString(dataAtual.getHour());
    	}
    	
    	return hora;
    }
    
    public static String minutoAtual()
    {
    	String minuto = "";
    	dataAtual = LocalDateTime.now();
    	
    	if(dataAtual.getMinute() < 10)
    	{
    		minuto = "0" + Integer.toString(dataAtual.getMinute());
    	}
    	else
    	{
    		minuto = Integer.toString(dataAtual.getMinute());
    	}
    	
    	return minuto;
    }

    public static String segundoAtual()
    {
    	String segundo = "";
    	dataAtual = LocalDateTime.now();
    	
    	if(dataAtual.getSecond() < 10)
    	{
    		segundo = "0" + Integer.toString(dataAtual.getSecond());
    	}
    	else
    	{
    		segundo = Integer.toString(dataAtual.getSecond());
    	}
    	
    	return segundo;
    }
    
    public static String dataFutura(int diaFuturos, int typeDate)
    {
    	String dia, mes, dataFutura = "";
    	
    	dataAtual = LocalDateTime.now().plusDays(diaFuturos);
    	mesAtual = dataAtual.getMonth().getValue();
    	
    	if(dataAtual.getDayOfMonth() < 10)
    	{
    		dia = "0" + dataAtual.getDayOfMonth();
    	}
    	else
    	{
    		dia = Integer.toString(dataAtual.getDayOfMonth());
    	}
    	
    	if(mesAtual < 10)
    	{
    		mes = "0" + mesAtual;
    	}
    	else
    	{
    		mes = Integer.toString(mesAtual);
    	}
    	
    	switch (typeDate) 
    	{
			case 0:
				dataFutura = dia + "/" + mes + "/" + Integer.toString(dataAtual.getYear());
				break;
	
			case 1:
				dataFutura = dia + "-" + mes + "-" + Integer.toString(dataAtual.getYear());
				break;
				
			case 2:
				dataFutura =  mes + "/" +  dia+ "/" + Integer.toString(dataAtual.getYear());
				break;
				
			case 3:
				dataFutura =  mes + "-" +  dia+ "-" + Integer.toString(dataAtual.getYear());
				break;
		}
    	
    	return dataFutura;
    }
    
    public static void deletarArquivo(String file)
    {
    	File report;
    	
    	report = new File(file);
    	if(report.exists()) report.delete();
    }
	
    public static String transformarDate(String text)
    {
    	if(text.length() == 8) return text.substring(0,2) + "/" + text.substring(2, 4) + "/" + text.substring(4, 8);
    	else return text;
    }
    
    //Methods Selenium
	public static WebElement GetElement(byElements byElements, String toFind)
    {
        switch (byElements)
        {
            case ClassName : //ClassName
                by = By.className(toFind);
                break;

            case CssSelector: //CssSelector
                by = By.cssSelector(toFind);
                break;

            case Id: //Id
                by = By.id(toFind);
                break;

            case LinkText: //LinkText
                by = By.linkText(toFind);
                break;

            case Name: //Name
                by = By.name(toFind);
                break;

            case PartialLinkText: //PartialLinkText
                by = By.partialLinkText(toFind);
                break;

            case TagName: //TagName
                by = By.tagName(toFind);
                break;

            case XPath: //XPath
                by = By.xpath(toFind);
                break;
        }

        return webDriver.findElement(by);
    }

	public static List<WebElement> GetElements(byElements byElements, String toFind)
    {
        switch (byElements)
        {
            case ClassName : //ClassName
                by = By.className(toFind);
                break;

            case CssSelector: //CssSelector
                by = By.cssSelector(toFind);
                break;

            case Id: //Id
                by = By.id(toFind);
                break;

            case LinkText: //LinkText
                by = By.linkText(toFind);
                break;

            case Name: //Name
                by = By.name(toFind);
                break;

            case PartialLinkText: //PartialLinkText
                by = By.partialLinkText(toFind);
                break;

            case TagName: //TagName
                by = By.tagName(toFind);
                break;

            case XPath: //XPath
                by = By.xpath(toFind);
                break;
        }

        return webDriver.findElements(by);
    }

	public static void Sendkeys(WebElement element, String text)
    {
        element.clear();
        element.sendKeys(text);
        extentReportsManager.SalvarLogScreenShotSucesso("Escrevendo no campo " + element.toString());
    }
	
	public static void Clicks(WebElement element)
    {
		extentReportsManager.SalvarLogScreenShotSucesso("Clicando no campo " + element.toString());
        element.click();
    }
	
	public static void ClicksJS(WebElement element)
	{
		extentReportsManager.SalvarLogScreenShotSucesso("Clicando no campo " + element.toString());
		JavascriptExecutor js = (JavascriptExecutor) baseTest.webDriver;
		js.executeScript("arguments[0].click()", element);
	}
	
	public static void ComboBoxSelect(WebElement element, String text)
    {
        Select selectElement = new Select(element);
        selectElement.selectByVisibleText(text);
        extentReportsManager.SalvarLogScreenShotSucesso("Selecionando no campo " + element.toString());
    }
	
	public static Boolean ValidacoesDeElementos(WebElement element)
	{
		if(element.isEnabled()) return true;
		else return false;
	}
}
