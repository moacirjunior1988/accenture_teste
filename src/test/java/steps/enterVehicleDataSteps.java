package steps;

import java.util.List;
import java.util.Map;

import com.aventstack.extentreports.ExtentTest;

import cucumber.api.DataTable;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import general.baseTest;
import general.extentReportsManager;
import general.hooks;
import general.testConfiguration;
import general.testMassa;
import pageObject.enterVehicleDataPO;

public class enterVehicleDataSteps 
{
	protected ExtentTest extentLog;
	private testMassa testMassa;
	private testConfiguration testConfiguration;
	private enterVehicleDataPO enterVehicleDataPO;
	
	public enterVehicleDataSteps(testMassa testMassa, testConfiguration testConfiguration, 
			enterVehicleDataPO enterVehicleDataPO)
	{
		this.testMassa = testMassa;
		this.testConfiguration = testConfiguration;
		this.extentLog =  hooks.extentTest;
		this.enterVehicleDataPO = enterVehicleDataPO;
	}
	
	@Dado("^que estou no site$")
	public void dadoQueEstouNoSite()
	{
		extentReportsManager.SalvarLogSucesso("Dado que estou no site");
		if(!enterVehicleDataPO.validarAExibicaoDaAbaEnterVehicleData()) baseTest.webDriver.quit();
		else extentReportsManager.SalvarLogScreenShotSucesso("Valida��o da exibi��o da aba enter vehicle data");
	}
	
	@E("^iniciarei o preenchendo o campo make$")
	public void eIniciareiOPreenchendoOCampoMake(DataTable dataTable)
	{
		extentReportsManager.SalvarLogSucesso("E iniciarei o preenchendo o campo make");
		List<Map<String,String>> data = dataTable.asMaps(String.class, String.class);
		
		enterVehicleDataPO.make(data.get(0).get("make"));
	}
	
	@E("^preencherei o campo model$")
	public void ePreenchereiOCampoModel(DataTable dataTable)
	{
		extentReportsManager.SalvarLogSucesso("E preencherei o campo model");
		List<Map<String,String>> data = dataTable.asMaps(String.class, String.class);
		
		enterVehicleDataPO.model(data.get(0).get("model"));
	}
	
	@E("^preencherei o campo cylinder capacity ccm$")
	public void ePreenchereiOCampoCylinderCapacityCcm(DataTable dataTable)
	{
		extentReportsManager.SalvarLogSucesso("E preencherei o campo cylinder capacity ccm");
		List<Map<String,String>> data = dataTable.asMaps(String.class, String.class);
		
		enterVehicleDataPO.cylinderCapacity(data.get(0).get("cylinder_capacity_ccm"));
	}
	
	@E("^preencherei o campo engine performance kw$")
	public void ePreenchereiOCampoEnginePerformanceKw(DataTable dataTable)
	{
		extentReportsManager.SalvarLogSucesso("E preencherei o campo engine performance kw");
		List<Map<String,String>> data = dataTable.asMaps(String.class, String.class);
		
		enterVehicleDataPO.enginePerformance(data.get(0).get("engine_performance_kw"));
	}
	
	@E("^preencherei o campo date of manufacture$")
	public void ePreenchereiOCampoDateOfManufacture(DataTable dataTable)
	{
		extentReportsManager.SalvarLogSucesso("E preencherei o campo date of manufacture");
		List<Map<String,String>> data = dataTable.asMaps(String.class, String.class);
		
		enterVehicleDataPO.dateOfManufacture(data.get(0).get("date_of_manufacture"));
	}
	
	@E("^preencherei o campo number of seats$")
	public void ePreenchereiOCampoNumberOfSeats(DataTable dataTable)
	{
		extentReportsManager.SalvarLogSucesso("E preencherei o campo number of seats");
		List<Map<String,String>> data = dataTable.asMaps(String.class, String.class);
		
		enterVehicleDataPO.numberOfSeats(data.get(0).get("number_of_seats"));
	}

	@E("^preencherei o campo right hand drive$")
	public void ePreenchereiOCampoRightHandDrive(DataTable dataTable)
	{
		extentReportsManager.SalvarLogSucesso("E preencherei o campo right hand drive");
		List<Map<String,String>> data = dataTable.asMaps(String.class, String.class);
		
		if(data.get(0).get("right_hand_drive").equals("sim")) enterVehicleDataPO.rightHandDriveYes();
		else enterVehicleDataPO.rightHandDriveNo();
	}
	
	@E("^preencherei o campo number of seats motorcycle$")
	public void ePreenchereiOCampoNumberOfSeatsMotorcycle(DataTable dataTable)
	{
		extentReportsManager.SalvarLogSucesso("E preencherei o campo number of seats motorcycle");
		List<Map<String,String>> data = dataTable.asMaps(String.class, String.class);
		
		if(Integer.parseInt(data.get(0).get("number_of_seats_motorcycle")) != 0) enterVehicleDataPO.numberOsSeatsMotorcycle(data.get(0).get("number_of_seats_motorcycle"));
	}
	
	@E("^preencherei o campo fuel type$")
	public void ePreenchereiOCampoFuelType(DataTable dataTable)
	{
		extentReportsManager.SalvarLogSucesso("E preencherei o campo fuel type");
		List<Map<String,String>> data = dataTable.asMaps(String.class, String.class);
		
		enterVehicleDataPO.fuelType(data.get(0).get("fuel_type"));
	}
	
	@E("^preencherei o campo payload kg$")
	public void ePreenchereiOCamposPayload(DataTable dataTable)
	{
		extentReportsManager.SalvarLogSucesso("E preencherei o campo payload kg");
		List<Map<String,String>> data = dataTable.asMaps(String.class, String.class);
		
		enterVehicleDataPO.payload(data.get(0).get("payload_kg"));
	}
	
	@E("^preencherei o campo total weight kg$")
	public void ePreenchereiOCampoTotalWeightKg(DataTable dataTable)
	{
		extentReportsManager.SalvarLogSucesso("E preencherei o campo total weight kg");
		List<Map<String,String>> data = dataTable.asMaps(String.class, String.class);
		
		enterVehicleDataPO.totalWeight(data.get(0).get("total_weight_kg"));
	}
	
	@E("^preencherei o campo list price$")
	public void ePreenchereiOCampoListPrice(DataTable dataTable)
	{
		extentReportsManager.SalvarLogSucesso("E preencherei o campo list price");
		List<Map<String,String>> data = dataTable.asMaps(String.class, String.class);
		
		enterVehicleDataPO.listPrice(data.get(0).get("list_price"));
	}
	
	@E("^preencherei o campo license plate number$")
	public void ePreenchereiOCampoLicensePlateNumber(DataTable dataTable)
	{
		extentReportsManager.SalvarLogSucesso("E preencherei o campo license plate number");
		List<Map<String,String>> data = dataTable.asMaps(String.class, String.class);
		
		enterVehicleDataPO.licensePlateNumber(data.get(0).get("license_plate_number"));
	}
	
	@E("^preencherei o campo annual mileage ml$")
	public void ePreenchereiOCampoAnnualMileagdeMl(DataTable dataTable)
	{
		extentReportsManager.SalvarLogSucesso("E preencherei o campo annual mileage ml");
		List<Map<String,String>> data = dataTable.asMaps(String.class, String.class);
		
		enterVehicleDataPO.annualMileage(data.get(0).get("annual_mileage_ml"));
	}
	
	@E("^clico em next enter vehicle data$")
	public void eClicoEmNextEnterVehicleData()
	{
		extentReportsManager.SalvarLogSucesso("E clico em next enter vehicle data");

		enterVehicleDataPO.next();
	}	
}
