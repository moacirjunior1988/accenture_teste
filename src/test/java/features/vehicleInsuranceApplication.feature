# language: pt
Funcionalidade: Validation Vehicle Insurance Application

  @vehicleInsuranceApplication @finalizado
  Cenario: Realizar o cadastro da aba enter vehicle data
    Dado que acesso o site
    Dado que estou no site
    E iniciarei o preenchendo o campo make
      | make |
      | Ford |
    E preencherei o campo model
      | model         |
      | Three-Wheeler |
    E preencherei o campo cylinder capacity ccm
      | cylinder_capacity_ccm |
      |                   145 |
    E preencherei o campo engine performance kw
      | engine_performance_kw |
      |                   1.2 |
    E preencherei o campo date of manufacture
      | date_of_manufacture |
      | 02/20/2004          |
    E preencherei o campo number of seats
      | number_of_seats |
      |               5 |
    E preencherei o campo right hand drive
      | right_hand_drive |
      | sim              |
    E preencherei o campo number of seats motorcycle
      | number_of_seats_motorcycle |
      |                          0 |
    E preencherei o campo fuel type
      | fuel_type |
      | Petrol    |
    E preencherei o campo payload kg
      | payload_kg |
      |        450 |
    E preencherei o campo total weight kg
      | total_weight_kg |
      |            1355 |
    E preencherei o campo list price
      | list_price |
      |   28000.00 |
    E preencherei o campo license plate number
      | license_plate_number |
      | NAL4005              |
    E preencherei o campo annual mileage ml
      | annual_mileage_ml |
      |             55000 |
    E clico em next enter vehicle data

  @vehicleInsuranceApplication @finalizado
  Cenario: Realizar o cadastro da aba enter insurant data
    Dado que estou na aba enter insurant data
    E iniciarei o preenchendo o campo first name
      | first_name |
      | Test       |
    E preencherei o campo last name
      | last_name |
      | Accenture |
    E preencherei o campo date of birth
      | date_of_birth |
      | 05/18/1985    |
    E preencherei o campo gender
      | gender |
      | male   |
    E preencherei o campo street address
      | stress_address |
      | Av: Brasil     |
    E preencherei o campo country
      | country |
      | Brazil  |
    E preencherei o campo zip code
      | zip_code |
      | 80010100 |
    E preencherei o campo city
      | city     |
      | Curitiba |
    E preencherei o campo occupation
      | occupation   |
      | Selfemployed |
    E preencherei o campo hobbies
      | hobbies                       |
      | Speeding,Other,Bungee Jumping |
    E preencherei o campo web site
      | web_site               |
      | https://www.uol.com.br |
    E preencherei o campo picture
      | picture |
      | 17.jpg  |
    E clico em next aba enter insurant data

  @vehicleInsuranceApplication @finalizado
  Cenario: Realizar o cadastro da aba enter product data
    Dado que estou na aba enter product data
    E iniciarei o preenchendo o campo start date
    E preencherei o campo insurance sum
      | insurance_sum |
      | 30.000.000,00 |
    E preencherei o campo merit rating
      | merit_rating |
      | Super Bonus  |
    E preencherei o campo damage insurance
      | damage_insurance |
      | No Coverage      |
    E preencherei o campo optional products
      | optional_products |
      | Euro Protection   |
    E preencherei o campo courtesy car
      | courtesy_car |
      | Yes          |
    E clico em next aba enter product data
