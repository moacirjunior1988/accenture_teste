package steps;

import java.util.List;
import java.util.Map;

import com.aventstack.extentreports.ExtentTest;

import cucumber.api.DataTable;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import general.baseTest;
import general.extentReportsManager;
import general.hooks;
import general.testConfiguration;
import general.testMassa;
import pageObject.enterProductDataPO;

public class enterProductDataSteps 
{
	protected ExtentTest extentLog;
	private testMassa testMassa;
	private testConfiguration testConfiguration;
	private enterProductDataPO enterProductDataPO;
	
	public enterProductDataSteps(testMassa testMassa, testConfiguration testConfiguration, 
			enterProductDataPO enterProductDataPO)
	{
		this.testMassa = testMassa;
		this.testConfiguration = testConfiguration;
		this.extentLog =  hooks.extentTest;
		this.enterProductDataPO = enterProductDataPO;
	}
	
	@Dado("^que estou na aba enter product data$")
	public void dadoQueEstouNoAbaEnterProductData()
	{
		extentReportsManager.SalvarLogSucesso("Dado que estou na aba enter product data");
		if(!enterProductDataPO.validarAExibicaoDaAbaEnterProductData()) baseTest.webDriver.quit();
		else extentReportsManager.SalvarLogScreenShotSucesso("Valida��o da exibi��o da aba enter product data");
	}
	
	@E("^iniciarei o preenchendo o campo start date$")
	public void eIniciareiOPreenchendoOCampoStartDate()
	{
		extentReportsManager.SalvarLogSucesso("E iniciarei o preenchendo o campo start date");
		
		enterProductDataPO.startDate(baseTest.dataFutura(40, 2));
	}
	
	@E("^preencherei o campo insurance sum$")
	public void ePreenchereiOCampoInsuranceSum(DataTable dataTable)
	{
		extentReportsManager.SalvarLogSucesso("E preencherei o campo insurance sum");
		List<Map<String,String>> data = dataTable.asMaps(String.class, String.class);
		
		enterProductDataPO.insuranceSu(data.get(0).get("insurance_sum"));
	}
	
	@E("^preencherei o campo merit rating$")
	public void ePreenchereiOCampoMeritRating(DataTable dataTable)
	{
		extentReportsManager.SalvarLogSucesso("E preencherei o campo merit rating");
		List<Map<String,String>> data = dataTable.asMaps(String.class, String.class);
		
		enterProductDataPO.meritRating(data.get(0).get("merit_rating"));
	}
	
	@E("^preencherei o campo damage insurance$")
	public void ePreenchereiOCampoDamageInsurance(DataTable dataTable)
	{
		extentReportsManager.SalvarLogSucesso("E preencherei o campo damage insurance");
		List<Map<String,String>> data = dataTable.asMaps(String.class, String.class);
		
		enterProductDataPO.damageInsurance(data.get(0).get("damage_insurance"));
	}
	
	@E("^preencherei o campo optional products$")
	public void ePreenchereiOCampoOptionalProducts(DataTable dataTable)
	{
		extentReportsManager.SalvarLogSucesso("E preencherei o campo optional products");
		List<Map<String,String>> data = dataTable.asMaps(String.class, String.class);
		
		String[] optionalProducts = data.get(0).get("optional_products").split(",");
		
		for(int a = 0; a <= (optionalProducts .length - 1); a++)
		{
			switch (optionalProducts[a]) 
			{
				case "Euro Protection":
					enterProductDataPO.optionalProductsEuroProtection();
					break;
					
				case "Legal Defense Insurance":
					enterProductDataPO.optionalProductsLegalDefenseInsurance();
					break;
			}
		}
	}
	
	@E("^preencherei o campo courtesy car$")
	public void ePreenchereiOCampoCourtesyCar(DataTable dataTable)
	{
		extentReportsManager.SalvarLogSucesso("E preencherei o campo courtesy car");
		List<Map<String,String>> data = dataTable.asMaps(String.class, String.class);
		
		enterProductDataPO.courtesyCar(data.get(0).get("courtesy_car"));
	}
	
	@E("^clico em next aba enter product data$")
	public void eClicoEmNextAbaEnterProductData()
	{
		extentReportsManager.SalvarLogSucesso("E clico em next aba enter product data");

		enterProductDataPO.next();
		
		baseTest.Sleep(5000);
	}
}
