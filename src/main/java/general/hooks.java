package general;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import gherkin.formatter.model.Step;

public class hooks 
{
	private static ExtentHtmlReporter htmlReporter;
	private static ExtentReports extentReport;
	public static ExtentTest extentTest;
	String path = System.getProperty("user.dir") + "/extentReport/htmlReporter.html";
	String pathPasta = System.getProperty("user.dir") + "/extentReport";
	
	@Before
	public void beforeCenario(Scenario scenario)
	{	
		if(scenario.getName().equals("Realizar o cadastro da aba enter vehicle data"))
		{			
			baseTest.criarPasta(pathPasta);
			
			baseTest.deletarArquivo(path);
		
			if(extentReport == null)
			{
				extentReport = new ExtentReports();
				htmlReporter =  new ExtentHtmlReporter(path);
				htmlReporter.config().setChartVisibilityOnOpen(true);
		        htmlReporter.config().setDocumentTitle("Accenture Test");
		        htmlReporter.config().setReportName("Accenture.TestReport");
		        htmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
		        htmlReporter.config().setTheme(Theme.STANDARD);
		        htmlReporter.config().setTimeStampFormat("dd/MM/yyyy HH:mm:ss");
				extentReport.attachReporter(htmlReporter);
			}
			extentTest = extentReport.createTest(scenario.getName());
		}
	}
	
	@After
	public void afterCenario(Scenario scenario)
	{
		if(scenario.getName().equals("Realizar o cadastro da aba enter product data"))
		{
			extentReport.flush();
			baseTest.webDriver.quit();
		}
	}
}