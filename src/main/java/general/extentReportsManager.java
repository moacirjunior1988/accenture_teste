package general;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.apache.commons.io.FileUtils;
import com.aventstack.extentreports.Status;

public class extentReportsManager extends baseTest
{
	//Variaveis para a criaca��o do arquivo de report em html.
	private static String nomeprint;
	private static String pathEvidence = System.getProperty("user.dir") + "/extentReport/";
	
	//Geradro de evid�ncia da execu��o
	private static String capture() throws IOException 
	{
		nomeprint = anoAtual() + "_" + mesAtual() + "_" + diaAtual() + "_" + horaAtual() + "_" + minutoAtual() + "_" + segundoAtual() + ".png";
		
		File scrFile = ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.FILE);
		File imagemDestino = new File(Paths.get(pathEvidence, nomeprint).toString());
		String errflpath = imagemDestino.getAbsolutePath();
		FileUtils. copyFile(scrFile, imagemDestino);
		nomeprint = null;
		
		return errflpath;
	}

	//Metodo de escrever no arquivo report com o status de sucesso.
	public static void SalvarLogSucesso(String text)
    {
        hooks.extentTest.pass(text);
    }

	//Metodo de escrever no arquivo report com o status de falha.
    public static void SalvarLogFalha(String text)
    {
    	hooks.extentTest.fail(text);
    }

  //Metodo de escrever no arquivo report com o status de informativo.
    public static void SalvarLogInformacoes(String text)
    {
    	hooks.extentTest.info(text);
    }
    
  //Metodo de escrever no arquivo report com o status de sucesso com evid�ncia.
    public static void SalvarLogScreenShotSucesso(String text)
    {
    	try {
			hooks.extentTest.log(Status.PASS, hooks.extentTest.addScreenCaptureFromPath(extentReportsManager.capture()) + text);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	//Metodo de escrever no arquivo report com o status de falha com evid�ncia.
    public static void SalvarLogScreenShotFalha(String text) throws IOException
    {
    	hooks.extentTest.log(Status.FAIL, hooks.extentTest.addScreenCaptureFromPath(extentReportsManager.capture()) + text);
    }

  //Metodo de escrever no arquivo report com o status de informativo com evid�ncia.
    public static void SalvarLogScreenShotInformacoes(String text) throws IOException
    {
    	hooks.extentTest.log(Status.INFO, hooks.extentTest.addScreenCaptureFromPath(extentReportsManager.capture()) + text);
    }
}
