package pageObject;

import org.openqa.selenium.WebElement;
import enums.byElements;
import general.baseTest;

public class enterInsurantDataPO extends baseTest
{
	//Elements
	private static WebElement txbFirstName = GetElement(byElements.Id, "firstname");
	private static WebElement txbLastName = GetElement(byElements.Id, "lastname");
	private static WebElement txbDateOfBirth = GetElement(byElements.Id, "birthdate");
	private static WebElement rbnGenderMale = GetElement(byElements.Id, "gendermale");
	private static WebElement rbnGenderFemale = GetElement(byElements.Id, "genderfemale");
	private static WebElement txbStreetAddress = GetElement(byElements.Id, "streetaddress");
	private static WebElement cbxCountry = GetElement(byElements.Id, "country");
	private static WebElement txbZipCode = GetElement(byElements.Id, "zipcode");
	private static WebElement txbCity = GetElement(byElements.Id, "city");
	private static WebElement cbxOccupation = GetElement(byElements.Id, "occupation");
	private static WebElement chbHobbiesSpeeding = GetElement(byElements.Id, "speeding");
	private static WebElement chbHobbiesBungeeJumping = GetElement(byElements.Id, "bungeejumping");
	private static WebElement chbHobbiesCliffDiving = GetElement(byElements.Id, "cliffdiving");
	private static WebElement chbHobbiesSkydiving = GetElement(byElements.Id, "skydiving");
	private static WebElement chbHobbiesOther = GetElement(byElements.Id, "other");	
	private static WebElement txbWebSite = GetElement(byElements.Id, "website");
	private static WebElement btnPictureOpen = GetElement(byElements.Id, "open");
	private static WebElement btnNext = GetElement(byElements.Id, "nextenterproductdata");
	private static WebElement btnPrev = GetElement(byElements.Id, "preventervehicledata");
	
	//Methods
	
	//SendKeys
	public static void firstName(String text)
	{
		Sendkeys(txbFirstName, text);
	}
	
	public static void lastName(String text)
	{
		Sendkeys(txbLastName, text);
	}
	
	public static void dateOfBirth(String text)
	{
		Sendkeys(txbDateOfBirth, text);
	}
	
	public static void streetAddress(String text)
	{
		Sendkeys(txbStreetAddress, text);
	}
	
	public static void zipCode(String text)
	{
		Sendkeys(txbZipCode, text);
	}
	
	public static void city(String text)
	{
		Sendkeys(txbCity, text);
	}
	
	public static void webSite(String text)
	{
		Sendkeys(txbWebSite, text);
	}

	// ComboBox
	public static void country(String text)
	{
		ComboBoxSelect(cbxCountry, text);
	}
	
	public static void occupation(String text)
	{
		ComboBoxSelect(cbxOccupation, text);
	}

	//RadioButton
	public static void genderMale()
	{
		ClicksJS(rbnGenderMale);
	}
	
	public static void genderFemale()
	{
		ClicksJS(rbnGenderFemale);
	}

	//CheckBox
	public static void hobbiesSpeeding()
	{
		ClicksJS(chbHobbiesSpeeding);
	}
	
	public static void hobbiesBungeeJumping()
	{
		ClicksJS(chbHobbiesBungeeJumping);
	}
	
	public static void hobbiesCliffDiving()
	{
		ClicksJS(chbHobbiesCliffDiving);
	}
	
	public static void hobbiesSkydiving()
	{
		ClicksJS(chbHobbiesSkydiving);
	}
	
	public static void hobbiesOther()
	{
		ClicksJS(chbHobbiesOther);
	}

	//Click
	public static void pictureOpen()
	{
		Clicks(btnPictureOpen);
	}
	
	public static void next()
	{
		Clicks(btnNext);
	}
	
	public static void prev()
	{
		Clicks(btnPrev);
	}

	//Validações
	public static Boolean validarAExibicaoDaAbaEnterInsurantData()
	{
		return baseTest.ValidacoesDeElementos(txbFirstName);
	}
}
